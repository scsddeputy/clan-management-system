## Fully Modular Clan Management System

### Issues

If you come across any issues please [report them here](https://bitbucket.org/scsddeputy/clan-management-system/issues).

### Contributing

Thank you for considering contributing to the Laravel Boilerplate project! Please feel free to make any pull requests, or start an issue to let mne know any feature request you would like to see in the future.

### Security Vulnerabilities

If you discover a security vulnerability within this boilerplate, please send an e-mail to Josh at scsddeputy@gmail.com, or create a pull request if possible. All security vulnerabilities will be promptly addressed. Please reference [this page](https://bitbucket.org/scsddeputy/clan-management-system/wiki/) to make sure you are up to date.
