<?php
namespace App\Applications\Dispatch\Http\Controllers;

use App\Applications\Dispatch\Repositories\DispatchContract;
use App\Applications\Dispatch\Repositories\IncidentContract;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class IncidentController extends Controller
{
    /**
     * @var IncidentContract
     */
    private $incident;
    /**
     * @var DispatchContract
     */
    private $dispatch;

    /**
     * IncidentController constructor.
     * @param IncidentContract $incident
     * @param DispatchContract $dispatch
     */
    public function __construct(IncidentContract $incident, DispatchContract $dispatch)
    {
        $this->incident = $incident;
        $this->dispatch = $dispatch;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dispatch::incidents.create')
            ->with('incident_types', $this->incident->getTypes())
            ->with('dispositions', $this->incident->getDispositions())
            ->with('units', $this->dispatch->getAvailableUnits())
            ->with('streets', $this->listify($this->incident->getStreets()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $incident = $this->incident->findOrThrowException($id);

        return view('dispatch::incidents.edit')
            ->with('incident_types', $this->incident->getTypes())
            ->with('dispositions', $this->incident->getDispositions())
            ->with('units', $this->dispatch->getAvailableUnits())
            ->with('streets', $this->listify($this->incident->getStreets()))
            ->with('incident', $incident);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function listify($input)
    {
        $select = [];
        foreach ($input as $item) {
            $select[] = $item->name;
        }
        return $select;
    }

}
