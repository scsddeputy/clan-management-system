<?php

namespace App\Applications\Dispatch\Http\Controllers;

use App\Applications\Dispatch\Http\Requests\IndexRequest;
use App\Applications\Dispatch\Models\Incident;
use App\Applications\Dispatch\Repositories\DispatchContract;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use yajra\Datatables\Datatables;


class DispatchController extends Controller
{
    /**
     * @var DispatchContract
     */
    private $dispatchRepo;

    /**
     * DispatchController constructor.
     * @param DispatchContract $dispatchRepo
     */
    public function __construct(DispatchContract $dispatchRepo)
    {
        $this->dispatchRepo = $dispatchRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(IndexRequest $request)
    {
        return view('dispatch::index')
            ->with('units', $this->dispatchRepo->getAllUnits())
            ->with('incident', $this->dispatchRepo->getActiveIncidents())
            ->with('available_units', $this->dispatchRepo->getAvailableUnits())
            ->with('unavailable_units', $this->dispatchRepo->getUnavailableUnits());
    }

    public function getActiveIncidents()
    {
        $incidents = Incident::with('street')->with('type')->with('units')->select('*');

        return Datatables::of($incidents)
            ->addColumn('location', function($incidents){
                return $incidents->street->name . ' / ' . $incidents->cross->name;
            })
            ->addColumn('type', function($incidents){
                return $incidents->type->name;
            })
            ->addColumn('options', function($incidents){
                return '<div class="dropdown">
                          <button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                            <i class="fa fa-cog"></i>
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="incidents/'. $incidents->id .'/edit">Update Call</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="incidents/'. $incidents->id .'/clearunits">Clear Units</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/incidents/'. $incidents->id .'/clearcall">Clear Call</a></li>
                          </ul>
                        </div>';
            })
            ->make(true);
    }
}
