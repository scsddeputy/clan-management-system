<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'dispatch'], function() {
	get('/', 'DispatchController@index');

	get('/getIncidents', ['as'=>'getActiveIncidents', 'uses'=>'DispatchController@getActiveIncidents']);
});

//Route::group(['prefix' => 'incidents'], function() {
//	get('create')
//})

Route::resource('incidents', 'IncidentController');