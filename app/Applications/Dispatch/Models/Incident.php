<?php

namespace App\Applications\Dispatch\Models;

use Illuminate\Database\Eloquent\Model;

class Incident extends Model {

	protected $table = 'dispatch_incidents';
	public $timestamps = true;
	protected $fillable = array('incident_type_id', 'street_id', 'cross_id', 'pd_dispatched', 'pd_arrived', 'pd_clear', 'fd_dispatched', 'fd_arrived', 'fd_clear', 'disposition', 'incident_cleared');
	protected $visible = array('id', 'incident_type_id', 'street_id', 'cross_id', 'pd_dispatched', 'pd_arrived', 'pd_clear', 'fd_dispatched', 'fd_arrived', 'fd_clear', 'disposition', 'incident_cleared', 'created_at');

	public function notes()
	{
		return $this->hasMany('App\Applications\Dispatch\Models\IncidentNote');
	}

	public function type()
	{
		return $this->belongsTo('App\Applications\Dispatch\Models\IncidentTypes', 'incident_type_id');
	}

	public function disposition()
	{
		return $this->belongsTo('App\Applications\Dispatch\Models\Dispositions', 'disposition_id');
	}

	public function street()
	{
		return $this->belongsTo('App\Applications\Dispatch\Models\Streets', 'street_id');
	}

	public function cross()
	{
		return $this->belongsTo('App\Applications\Dispatch\Models\Streets', 'cross_id');
	}

	public function units()
	{
		return $this->hasManyThrough('App\Applications\Dispatch\Models\Units', 'App\Applications\Dispatch\Models\IncidentUnits', 'incident_id', 'id');
	}

	public function scopeActiveCalls($query)
	{
		return $query->where('incident_cleared', 0);
	}

}