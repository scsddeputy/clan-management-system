<?php

namespace App\Applications\Dispatch\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RadioLog extends Model {

	protected $table = 'dispatch_radio_log';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];
	protected $fillable = array('user_id', 'message');
	protected $visible = array('user_id', 'message');

	public function user()
	{
		return $this->belongsTo('App\User');
	}

}