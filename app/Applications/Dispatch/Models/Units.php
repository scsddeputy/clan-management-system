<?php

namespace App\Applications\Dispatch\Models;

use Illuminate\Database\Eloquent\Model;

class Units extends Model
{

	protected $table = 'dispatch_units';
	public $timestamps = true;
	protected $fillable = array('unit_number', 'division_id', 'call_status', 'duty_status');
	protected $visible = array('unit_number', 'division_id', 'call_status', 'duty_status');

	public function division()
	{
		return $this->belongsTo('App\Applications\Dispatch\Models\Divisions');
	}

	public function scopeOnDuty($query)
	{
		return $query->where('duty_status', 1);
	}

	public function scopeAvailable($query)
	{
		return $query->where('call_status', 0);
	}

	public function scopeUnavailable($query)
	{
		return $query->where('call_status', 1);
	}

}