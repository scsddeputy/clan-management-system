<?php

namespace App\Applications\Dispatch\Models;

use Illuminate\Database\Eloquent\Model;

class NcicRecord extends Model {

	protected $table = 'dispatch_ncic_records';
	public $timestamps = true;
	protected $fillable = array('person_id', 'user_id', 'record_type', 'record_info', 'record_closed');
	protected $visible = array('person_id', 'user_id', 'record_type', 'record_info', 'record_closed');

}