<?php

namespace App\Applications\Dispatch\Models;

use Illuminate\Database\Eloquent\Model;

class Divisions extends Model {

	protected $table = 'dispatch_divisions';
	public $timestamps = true;
	protected $fillable = array('name');
	protected $visible = array('name');

	public function units()
	{
		return $this->hasMany('App\Applications\Dispatch\Models\Units');
	}

}