<?php

namespace App\Applications\Dispatch\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model {

	protected $table = 'dispatch_persons';
	public $timestamps = true;

}