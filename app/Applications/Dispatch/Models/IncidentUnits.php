<?php

namespace App\Applications\Dispatch\Models;

use Illuminate\Database\Eloquent\Model;

class IncidentUnits extends Model {

	protected $table = 'dispatch_incident_units';
	public $timestamps = true;
	protected $fillable = array('incident_id', 'unit_id');
	protected $visible = array('incident_id', 'unit_id');

	public function incident()
	{
		return $this->belongsTo('App\Applications\Dispatch\Models\Incident');
	}

	public function units()
	{
		return $this->hasMany('App\Applications\Dispatch\Models\Units');
	}

}