<?php

namespace App\Applications\Dispatch\Models;

use Illuminate\Database\Eloquent\Model;

class Dispositions extends Model {

	protected $table = 'dispatch_dispositions';
	public $timestamps = true;
	protected $fillable = array('name', 'user_id');
	protected $visible = array('id', 'name', 'user_id');

}