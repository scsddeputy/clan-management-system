<?php

namespace App\Applications\Dispatch\Models;

use Illuminate\Database\Eloquent\Model;

class Islands extends Model {

	protected $table = 'dispatch_islands';
	public $timestamps = true;
	protected $fillable = array('name');
	protected $visible = array('name');

}