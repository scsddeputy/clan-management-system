<?php

namespace App\Applications\Dispatch\Models;

use Illuminate\Database\Eloquent\Model;

class Streets extends Model {

	protected $table = 'dispatch_streets';
	public $timestamps = true;
	protected $fillable = array('name', 'island_id');
	protected $visible = array('name', 'island_id');

	public function island()
	{
		return $this->belongsTo('App\Applications\Dispatch\Models\Islands');
	}

}