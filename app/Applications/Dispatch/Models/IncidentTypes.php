<?php

namespace App\Applications\Dispatch\Models;

use Illuminate\Database\Eloquent\Model;

class IncidentTypes extends Model {

	protected $table = 'dispatch_incident_types';
	public $timestamps = true;
	protected $fillable = array('name');
	protected $visible = array('id','name');

}