<?php

namespace App\Applications\Dispatch\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IncidentNote extends Model {

	protected $table = 'dispatch_incident_notes';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];
	protected $fillable = array('incident_id', 'user_id', 'note');
	protected $visible = array('incident_id', 'user_id', 'note');

	public function incident()
	{
		return $this->belongsTo('App\Applications\Dispatch\Models\Incident');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

}