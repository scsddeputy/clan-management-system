<?php
/**
 * Created by PhpStorm.
 * User: jleav
 * Date: 12/7/2015
 * Time: 3:43 AM
 */

namespace App\Applications\Dispatch\Repositories;


interface DispatchContract
{
    public function findOrThrowException($id);

    public function getAllUnits();

    public function getActiveIncidents();

    public function getAvailableUnits();

    public function getUnavailableUnits();
}