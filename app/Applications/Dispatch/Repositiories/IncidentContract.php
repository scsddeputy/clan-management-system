<?php
/**
 * User: Josh
 * Date: 12/16/2015
 * Time: 7:22 PM
 */

namespace App\Applications\Dispatch\Repositories;


interface IncidentContract
{
    public function findOrThrowException($id);

    public function getTypes();

    public function getDispositions();

    public function getStreets();
}