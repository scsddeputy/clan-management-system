<?php
/**
 * User: Josh
 * Date: 12/16/2015
 * Time: 7:23 PM
 */

namespace App\Applications\Dispatch\Repositories;


use App\Applications\Dispatch\Models\Dispositions;
use App\Applications\Dispatch\Models\Incident;
use App\Applications\Dispatch\Models\IncidentTypes;
use App\Applications\Dispatch\Models\Streets;
use App\Exceptions\GeneralException;

class IncidentRepository implements IncidentContract
{

    public function findOrThrowException($id)
    {
        $incident = Incident::find($id);
        if (! is_null($incident)) return $incident;
        throw new GeneralException('That goal does not exist.');
    }

    public function getTypes()
    {
        return IncidentTypes::all();
    }

    public function getDispositions()
    {
        return Dispositions::all();
    }

    public function getStreets()
    {
        return Streets::all();
    }
}