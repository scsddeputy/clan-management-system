<?php
/**
 * Created by PhpStorm.
 * User: jleav
 * Date: 12/7/2015
 * Time: 3:45 AM
 */

namespace App\Applications\Dispatch\Repositories;


use App\Applications\Dispatch\Models\Incident;
use App\Applications\Dispatch\Models\Units;

class DispatchRepository implements DispatchContract
{

    public function findOrThrowException($id)
    {
        // TODO: Implement findOrThrowException() method.
    }

    public function getActiveIncidents()
    {
        return Incident::ActiveCalls()->get();
    }

    public function getAllUnits()
    {
        return Units::all();
    }

    public function getAvailableUnits()
    {
        return Units::OnDuty()->Available()->get();
    }

    public function getUnavailableUnits()
    {
        return Units::OnDuty()->Unavailable()->get();
    }
}