<?php
namespace App\Applications\Dispatch\Providers;

use App;
use App\Applications\Dispatch\Repositories\DispatchContract;
use App\Applications\Dispatch\Repositories\DispatchRepository;
use App\Applications\Dispatch\Repositories\IncidentContract;
use App\Applications\Dispatch\Repositories\IncidentRepository;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class DispatchServiceProvider extends ServiceProvider
{
	/**
	 * Register the Dispatch module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Applications\Dispatch\Providers\RouteServiceProvider');

		$this->registerNamespaces();
		$this->registerRepos();
	}

	/**
	 * Register the Dispatch module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('dispatch', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('dispatch', realpath(__DIR__.'/../Resources/Views'));
	}

	protected function registerRepos()
	{
		$this->app->bind(
				DispatchContract::class,
				DispatchRepository::class
		);

		$this->app->bind(
			IncidentContract::class,
			IncidentRepository::class
		);
	}
}
