@extends('frontend.layouts.master')

@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
@stop
@section('content')
    <div>
        <table id="incidents" class="tablesorter table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Date</th>
                <th>Location</th>
                <th>Type</th>
                <th>Options</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
    &nbsp;
    <div class="row">
        <div class="col-md-4">
            <table id="unitsavail" class="table table-condensed">
                <caption><b>Units Available</b></caption>
                <thead>
                <tr>
                    <th>Unit #</th>
                    <th>Division</th>
                </tr>
                </thead>
                <tbody>
                @foreach($available_units as $unit)
                    <tr>
                        <td>{!! $unit['unit_number'] !!}</td>
                        <td>{!! $unit->division->name !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!--Assign Units Modal -->
        @include('dispatch::partials.unitsModal')
                <!--Assign Units Modal -->
        <!--NCIC Modal -->
        @yield('ncic')
                <!--NCIC Modal -->
        <!--Search Modal -->
        @yield('search')
                <!--Search Modal -->
            <div class="col-md-4">
              <div class="row">
                <div class="col-md-6">
                  <a href="incidents/create" class="btn btn-block btn-primary" role="button">Add Call</a>
                  <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#search">Search</button>
                  <button class="btn btn-block btn-primary" disabled="disabled">Radio Log #ComingSoon</button>
                </div>
                <div class="col-md-6">
                  <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#unitAssign">Assign Unit</button>
                  <a href="incidents/history" class="btn btn-block btn-primary" role="button">Call History</a>
                  <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#ncic">NCIC</button>
                </div>
              </div>
            </div>
        <div class="col-md-4">
            <table id="unitsunavail" class="table table-condensed">
                <caption><b>Units Unavailable</b></caption>
                <thead>
                <tr>
                    <th>Unit #</th>
                    <th>Division</th>
                </tr>
                </thead>
                <tbody>
                @foreach($unavailable_units as $unit)
                    <tr>
                        <td>{!! $unit['unit_number'] !!}</td>
                        <td>{!! $unit->division->name !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('js')
        <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script>
        $(function() {
            $('#incidents').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('getActiveIncidents') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'created_at', name: 'date'},
                    { data: 'location', name: 'location'},
                    { data: 'type', name: 'type'},
                    { data: 'options', name: 'options' }
                ],
                "info": false,
                "scrollY": "400px",
                "dom": "frtiS",
                searching: false
            });
        });
    </script>
@stop