<div class="modal fade" id="unitAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Assign Units</h4>
            </div>
            <div class="modal-body">
                <!--Inside of Modal-->
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Select Units</label>
                            <input type="text" class="form-control" placeholder="Search Unit Num" id="searchunit">
                            &nbsp;
                            <div class="controls">

                                <select multiple class="form-control" name="unitNum[]" id="units" required>
                                    @foreach($units as $unit)
                                        <option>{{ $unit->unit_number }}</option>
                                    @endforeach
                                </select>&nbsp;
                                <input type="text" class="form-control" placeholder="Assign Beat" name="beat">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="controls">
                                <input type="submit" class="btn btn-block btn-primary" name="submitButton" value="Set On Duty">
                                <input type="submit" class="btn btn-block btn-primary" name="submitButton" value="Set Off Duty">
                                <input type="submit" class="btn btn-block btn-primary" name="submitButton" value="Set All Off Duty">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="controls">
                                <input type="submit" class="btn btn-block btn-primary" name="submitButton" value="Update Beat">
                                <input type="submit" class="btn btn-block btn-primary" name="submitButton" value="Set Unit Busy">
                                <input type="submit" class="btn btn-block btn-primary" name="submitButton" value="Set Unit Avail">

                            </div>
                        </div>
                    </div>
                </div>
                <!--Inside of Modal-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>