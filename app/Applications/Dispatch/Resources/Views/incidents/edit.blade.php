@extends('frontend.layouts.master')

@section('css')
    <style>
        .typeahead,
        .tt-query,
        .tt-hint {
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }

        .twitter-typeahead {
            width: 100%;
        }

        .typeahead {
            background-color: #fff;
        }

        .typeahead:focus {
            border: 2px solid #0097cf;
        }

        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;

        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }
    </style>
@stop

@section('content')

    {!! Form::open(array('route' => ['incidents.update', $incident->id], 'method'=>'put')) !!}

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('date', 'Date:')!!}
                <div class="controls">
                    {!! Form::text('date', substr($incident['created_at'], 0, 10), array("readonly", "class" => "form-control"))!!}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('time', 'Time:')!!}
                <div class="controls">
                    {!! Form::text('time', substr($incident['created_at'], 11, 8), array("readonly", "class" => "form-control"))!!}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('incidenttype', 'Incident Type:')!!}
                <div class="controls">
                    {!! Form::select('incident_type', $incident_types->lists('name', 'id'), $incident->type, ['class'=>'form-control']) !!}
                </div>
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('street', 'Street:')!!}
                <div class="controls">
                    {!! Form::text('street', $incident->street->name, array("class" => "form-control typeahead"))!!}
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('cross', 'Cross Street:')!!}
                <div class="controls">
                    {!! Form::text('cross', $incident->cross->name, array("class" => "form-control typeahead"))!!}
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('disposition', 'Disposition:')!!}
                <div class="controls">
                    {!! Form::select('disposition', $dispositions->lists('name', 'id'), $incident->disposition, ['class'=>'form-control']) !!}
                </div>
            </div>
        </div>
    </div><br>
    {!! Form::label('notes', 'Incident Information:')!!}
    <div class="row">
        <div class="col-md-12">
            {!! Form::textarea('notes', null, array( "class"=>"form-control input-block-level", "style"=>"margin: 0px -2px 0px 0px; height: 103px;"))!!}
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-5">
            <table class="table table-hover table-condensed">
                <thead>
                <tr>
                    <th>Unit #</th>
                    <th>Beat</th>
                    <th>Officer Name</th>
                </tr>
                </thead>
                <tbody>
                @foreach($units as $unit)
                    @if($unit->status == 0 && $unit->duty_status == 1)
                        <tr>
                            <td>{!! $unit->unit_number !!}</td>
                            <td>{!! $unit->beat !!}</td>
                            <td>{!! $unit->name !!}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('primary', 'Primary Officer:')!!}
                        <div class="controls">
                            {!! Form::text('primary', null, array("class" => "form-control"))!!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('additional', 'Secondary Officer:')!!}
                        <div class="controls">
                            {!! Form::text('additional', null, array("class" => "form-control"))!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('dispatched', 'Dispatched:')!!}
                        <div class="controls">
                            @if($incident->pd_dispatched == null)
                                {!! Form::checkbox('pd_dispatched', 1, 0) !!}
                            @else
                                <i class="fa fa-check"></i> <span class="badge"> {!! $incident['pd_dispatched'] !!}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('arrived', 'Arrived:')!!}
                        <div class="controls">
                            @if($incident->pd_arrived == null)
                                {!! Form::checkbox('pd_arrived', 1, 0) !!}
                            @else
                                <i class="fa fa-check"></i> <span class="badge"> {!! $incident['pd_arrived'] !!}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('cleared', 'Cleared:')!!}
                        <div class="controls">
                            @if($incident->pd_clear == null)
                                {!! Form::checkbox('pd_clear', 1, 0) !!}
                            @else
                                <i class="fa fa-check"></i> <span class="badge"> {!! $incident['pd_clear'] !!}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('fdispatched', 'Fire Dispatched:')!!}
                        <div class="controls">
                            @if($incident->fd_dispatched == null)
                                {!! Form::checkbox('fd_dispatched', 1, 0) !!}
                            @else
                                <i class="fa fa-check"></i> <span class="badge"> {!! $incident['fd_dispatched'] !!}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('farrived', 'Fire Arrived:')!!}
                        <div class="controls">
                            @if($incident->fd_arrived == null)
                                {!! Form::checkbox('fd_arrived', 1, 0) !!}
                            @else
                                <i class="fa fa-check"></i> <span class="badge"> {!! $incident['fd_arrived'] !!}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('fcleared', 'Fire Cleared:')!!}
                        <div class="controls">
                            @if($incident->fd_clear == null)
                                {!! Form::checkbox('fd_clear', 1, 0) !!}
                            @else
                                <i class="fa fa-check"></i> <span class="badge"> {!! $incident['fd_clear'] !!}</span>
                            @endif
                        </div>
                    </div>
                    {!! Form::submit('Submit', array("class"=>"btn btn-primary"))!!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('js')
    <script src="/js/plugins/typeahead/typeahead.js"></script>

    <script>


        //////////////////////////////

        var substringMatcher = function(strs) {
            return function findMatches(q, cb) {
                var matches, substringRegex;

                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strs, function(i, str) {
                    if (substrRegex.test(str)) {
                        matches.push(str);
                    }
                });

                cb(matches);
            };
        };

        var states = {!! json_encode($streets) !!};

        $('.typeahead').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'states',
                    source: substringMatcher(states)
                });
    </script>
@stop