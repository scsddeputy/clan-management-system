<?php
/**
 * Copyright StreamersIO LLC.
 * User: Josh
 * Date: 12/6/2015
 * Time: 4:36 AM
 */

namespace App\Applications\Dispatch\Database\Seeds;


use App\Applications\Dispatch\Models\Islands;
use Illuminate\Database\Seeder;

class IslandTableSeeder extends Seeder
{
    public function run()
    {
        Islands::create([
            'name' => 'Liberty City'
        ]);

        Islands::create([
            'name' => 'Los Santos'
        ]);
    }
}