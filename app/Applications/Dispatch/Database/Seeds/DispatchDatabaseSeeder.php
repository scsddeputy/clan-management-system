<?php
namespace App\Applications\Dispatch\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DispatchDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call(IslandTableSeeder::class);
		$this->call(StreetTableSeeder::class);
		$this->call(DispositionTableSeeder::class);
		$this->call(DivisionsTableSeeder::class);
		$this->call(IncidentTypeTableSeeder::class);
		$this->call(PermissionsTableSeeder::class);

		Model::reguard();
	}

}
