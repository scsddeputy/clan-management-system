<?php
/**
 * Copyright StreamersIO LLC.
 * User: Josh
 * Date: 12/12/2015
 * Time: 12:01 PM
 */

namespace App\Applications\Dispatch\Database\Seeds;


use App\Applications\Dispatch\Http\Controllers\DispatchController;
use App\Applications\Dispatch\Models\Divisions;
use Illuminate\Database\Seeder;

class DivisionsTableSeeder extends Seeder
{
    public function run()
    {
        Divisions::create([
            'name' => 'Police'
        ]);

        Divisions::create([
            'name' => 'Sheriff'
        ]);
    }
}