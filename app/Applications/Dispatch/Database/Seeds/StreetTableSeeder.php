<?php
/**
 * Copyright StreamersIO LLC.
 * User: Josh
 * Date: 11/6/1015
 * Time: 4:16 AM
 */

namespace App\Applications\Dispatch\Database\Seeds;


use App\Applications\Dispatch\Models\Streets;
use Illuminate\Database\Seeder;

class StreetTableSeeder extends Seeder
{
    public function run()
    {
        // Anvil Ave
        Streets::create(array(
            'name' => 'Anvil Ave',
            'island_id' => 1
        ));

        // Applewhite St
        Streets::create(array(
            'name' => 'Applewhite St',
            'island_id' => 1
        ));

        // Argus St
        Streets::create(array(
            'name' => 'Argus St',
            'island_id' => 1
        ));

        // Asahara Rd
        Streets::create(array(
            'name' => 'Asahara Rd',
            'island_id' => 1
        ));

        // Aspdin Drive
        Streets::create(array(
            'name' => 'Aspdin Drive',
            'island_id' => 1
        ));

        // Babbage Drive
        Streets::create(array(
            'name' => 'Babbage Drive',
            'island_id' => 1
        ));

        // Bear St
        Streets::create(array(
            'name' => 'Bear St',
            'island_id' => 1
        ));

        // Beaverhead Ave
        Streets::create(array(
            'name' => 'Beaverhead Ave',
            'island_id' => 1
        ));

        // Bedrock St
        Streets::create(array(
            'name' => 'Bedrock St',
            'island_id' => 1
        ));

        // Big Horn Drive
        Streets::create(array(
            'name' => 'Big Horn Drive',
            'island_id' => 1
        ));

        // Bowline (Bergenline)
        Streets::create(array(
            'name' => 'Bowline (Bergenline)',
            'island_id' => 1
        ));

        // Boyden Ave
        Streets::create(array(
            'name' => 'Boyden Ave',
            'island_id' => 1
        ));

        // Bridger St
        Streets::create(array(
            'name' => 'Bridger St',
            'island_id' => 1
        ));

        // Cariboo Ave
        Streets::create(array(
            'name' => 'Cariboo Ave',
            'island_id' => 1
        ));

        // Catskill Ave
        Streets::create(array(
            'name' => 'Catskill Ave',
            'island_id' => 1
        ));

        // Cockerell Ave
        Streets::create(array(
            'name' => 'Cockerell Ave',
            'island_id' => 1
        ));

        // Drebbel (Drebbel Dr)
        Streets::create(array(
            'name' => 'Drebbel (Drebbel Dr)',
            'island_id' => 1
        ));

        // Edison Ave
        Streets::create(array(
            'name' => 'Edison Ave',
            'island_id' => 1
        ));

        // Emery St
        Streets::create(array(
            'name' => 'Emery St',
            'island_id' => 1
        ));

        // Farnsworth Rd
        Streets::create(array(
            'name' => 'Farnsworth Rd',
            'island_id' => 1
        ));

        // Flathead Rd
        Streets::create(array(
            'name' => 'Flathead Rd',
            'island_id' => 1
        ));

        // Fleming St
        Streets::create(array(
            'name' => 'Fleming St',
            'island_id' => 1
        ));

        // Fulcrum Ave
        Streets::create(array(
            'name' => 'Fulcrum Ave',
            'island_id' => 1
        ));

        // Grenadier St
        Streets::create(array(
            'name' => 'Grenadier St',
            'island_id' => 1
        ));

        // Grommet St
        Streets::create(array(
            'name' => 'Grommet St',
            'island_id' => 1
        ));

        // Hardtack Ave
        Streets::create(array(
            'name' => 'Hardtack Ave',
            'island_id' => 1
        ));

        // Hubbard Ave
        Streets::create(array(
            'name' => 'Hubbard Ave',
            'island_id' => 1
        ));

        // Ivy Rd
        Streets::create(array(
            'name' => 'Ivy Rd',
            'island_id' => 1
        ));

        // Julin Ave
        Streets::create(array(
            'name' => 'Julin Ave',
            'island_id' => 1
        ));

        // Jonestown Avenue
        Streets::create(array(
            'name' => 'Jonestown Avenue',
            'island_id' => 1
        ));

        // Kemeny St
        Streets::create(array(
            'name' => 'Kemeny St',
            'island_id' => 1
        ));

        // Keneckie Ave
        Streets::create(array(
            'name' => 'Keneckie Ave',
            'island_id' => 1
        ));

        // Koresh Square
        Streets::create(array(
            'name' => 'Koresh Square',
            'island_id' => 1
        ));

        // Latchkey Ave
        Streets::create(array(
            'name' => 'Latchkey Ave',
            'island_id' => 1
        ));

        // Lemhi St
        Streets::create(array(
            'name' => 'Lemhi St',
            'island_id' => 1
        ));

        // Lockowski Ave
        Streets::create(array(
            'name' => 'Lockowski Ave',
            'island_id' => 1
        ));

        // Long John Ave
        Streets::create(array(
            'name' => 'Long John Ave',
            'island_id' => 1
        ));

        // Lyndon Ave
        Streets::create(array(
            'name' => 'Lyndon Ave',
            'island_id' => 1
        ));

        // Lee Road
        Streets::create(array(
            'name' => 'Lee Road',
            'island_id' => 1
        ));

        // Mahesh Ave
        Streets::create(array(
            'name' => 'Mahesh Ave',
            'island_id' => 1
        ));

        // Mandrel Rd
        Streets::create(array(
            'name' => 'Mandrel Rd',
            'island_id' => 1
        ));

        // Manzano Rd
        Streets::create(array(
            'name' => 'Manzano Rd',
            'island_id' => 1
        ));

        // Moog St
        Streets::create(array(
            'name' => 'Moog St',
            'island_id' => 1
        ));

        // Muskteer Ave
        Streets::create(array(
            'name' => 'Muskteer Ave',
            'island_id' => 1
        ));

        // Myung (Myung Ln)
        Streets::create(array(
            'name' => 'Myung (Myung Ln)',
            'island_id' => 1
        ));

        // Mueri Streets
        Streets::create(array(
            'name' => 'Mueri Streets',
            'island_id' => 1
        ));

        // Niblick St
        Streets::create(array(
            'name' => 'Niblick St',
            'island_id' => 1
        ));

        // Nougat St
        Streets::create(array(
            'name' => 'Nougat St',
            'island_id' => 1
        ));

        // Odhner Ave
        Streets::create(array(
            'name' => 'Odhner Ave',
            'island_id' => 1
        ));

        // Ortiz Rd
        Streets::create(array(
            'name' => 'Ortiz Rd',
            'island_id' => 1
        ));

        // Owl Creek Ave
        Streets::create(array(
            'name' => 'Owl Creek Ave',
            'island_id' => 1
        ));

        // Panhandle Rd
        Streets::create(array(
            'name' => 'Panhandle Rd',
            'island_id' => 1
        ));

        // Percell Rd
        Streets::create(array(
            'name' => 'Percell Rd',
            'island_id' => 1
        ));

        // Phalanx Rd
        Streets::create(array(
            'name' => 'Phalanx Rd',
            'island_id' => 1
        ));

        // Plumbbob Ave
        Streets::create(array(
            'name' => 'Plumbbob Ave',
            'island_id' => 1
        ));

        // Plumbers Skyway
        Streets::create(array(
            'name' => 'Plumbers Skyway',
            'island_id' => 1
        ));

        // Praetorian Ave
        Streets::create(array(
            'name' => 'Praetorian Ave',
            'island_id' => 1
        ));

        // Rael Ave
        Streets::create(array(
            'name' => 'Rael Ave',
            'island_id' => 1
        ));

        // Rand Ave
        Streets::create(array(
            'name' => 'Rand Ave',
            'island_id' => 1
        ));

        // Red Wing Ave
        Streets::create(array(
            'name' => 'Red Wing Ave',
            'island_id' => 1
        ));

        // Roebuck Rd
        Streets::create(array(
            'name' => 'Roebuck Rd',
            'island_id' => 1
        ));

        // Sacramento Ave
        Streets::create(array(
            'name' => 'Sacramento Ave',
            'island_id' => 1
        ));

        // Schneider Ave
        Streets::create(array(
            'name' => 'Schneider Ave',
            'island_id' => 1
        ));

        // Sculpin Ave
        Streets::create(array(
            'name' => 'Sculpin Ave',
            'island_id' => 1
        ));

        // Sinclair Avenue
        Streets::create(array(
            'name' => 'Sinclair Avenue',
            'island_id' => 1
        ));

        // Storax Rd
        Streets::create(array(
            'name' => 'Storax Rd',
            'island_id' => 1
        ));

        // Strower Ave
        Streets::create(array(
            'name' => 'Strower Ave',
            'island_id' => 1
        ));

        // Tenmile St
        Streets::create(array(
            'name' => 'Tenmile St',
            'island_id' => 1
        ));

        // Tinderbox Ave
        Streets::create(array(
            'name' => 'Tinderbox Ave',
            'island_id' => 1
        ));

        // Toggle Ave
        Streets::create(array(
            'name' => 'Toggle Ave',
            'island_id' => 1
        ));

        // Traeger Rd
        Streets::create(array(
            'name' => 'Traeger Rd',
            'island_id' => 1
        ));

        // Trinity Rd
        Streets::create(array(
            'name' => 'Trinity Rd',
            'island_id' => 1
        ));

        // Vitullo Ave
        Streets::create(array(
            'name' => 'Vitullo Ave',
            'island_id' => 1
        ));

        // Albany Ave
        Streets::create(array(
            'name' => 'Albany Ave',
            'island_id' => 1
        ));

        // Amethyst St
        Streets::create(array(
            'name' => 'Amethyst St',
            'island_id' => 1
        ));

        // Amsterdam Lane
        Streets::create(array(
            'name' => 'Amsterdam Lane',
            'island_id' => 1
        ));

        // Astoria Streets
        Streets::create(array(
            'name' => 'Astoria Streets',
            'island_id' => 1
        ));

        // Barium St
        Streets::create(array(
            'name' => 'Barium St',
            'island_id' => 1
        ));

        // Bismarck Ave
        Streets::create(array(
            'name' => 'Bismarck Ave',
            'island_id' => 1
        ));

        // Boleyn St
        Streets::create(array(
            'name' => 'Boleyn St',
            'island_id' => 1
        ));

        // Borlock Road
        Streets::create(array(
            'name' => 'Borlock Road',
            'island_id' => 1
        ));

        // Bridge Lane North
        Streets::create(array(
            'name' => 'Bridge Lane North',
            'island_id' => 1
        ));

        // Bridge Lane South
        Streets::create(array(
            'name' => 'Bridge Lane South',
            'island_id' => 1
        ));

        // Brown Place
        Streets::create(array(
            'name' => 'Brown Place',
            'island_id' => 1
        ));

        // Burlesque
        Streets::create(array(
            'name' => 'Burlesque',
            'island_id' => 1
        ));

        // Bus Lane
        Streets::create(array(
            'name' => 'Bus Lane',
            'island_id' => 1
        ));

        // Calcium St
        Streets::create(array(
            'name' => 'Calcium St',
            'island_id' => 1
        ));

        // Castle Drive
        Streets::create(array(
            'name' => 'Castle Drive',
            'island_id' => 1
        ));

        // Castle Tunnel
        Streets::create(array(
            'name' => 'Castle Tunnel',
            'island_id' => 1
        ));

        // Cavity Lane
        Streets::create(array(
            'name' => 'Cavity Lane',
            'island_id' => 1
        ));

        // Cod Row
        Streets::create(array(
            'name' => 'Cod Row',
            'island_id' => 1
        ));

        // Columbus Ave
        Streets::create(array(
            'name' => 'Columbus Ave',
            'island_id' => 1
        ));

        // Denver Ave
        Streets::create(array(
            'name' => 'Denver Ave',
            'island_id' => 1
        ));

        // Denver-Exeter Ave
        Streets::create(array(
            'name' => 'Denver-Exeter Ave',
            'island_id' => 1
        ));

        // Diamond St
        Streets::create(array(
            'name' => 'Diamond St',
            'island_id' => 1
        ));

        // Emerald Streets
        Streets::create(array(
            'name' => 'Emerald Streets',
            'island_id' => 1
        ));

        // Ersatz Row
        Streets::create(array(
            'name' => 'Ersatz Row',
            'island_id' => 1
        ));

        // Exeter Ave
        Streets::create(array(
            'name' => 'Exeter Ave',
            'island_id' => 1
        ));

        // Feldspar
        Streets::create(array(
            'name' => 'Feldspar',
            'island_id' => 1
        ));

        // Flatfish Place
        Streets::create(array(
            'name' => 'Flatfish Place',
            'island_id' => 1
        ));

        // Frankfort Ave
        Streets::create(array(
            'name' => 'Frankfort Ave',
            'island_id' => 1
        ));

        // Galveston Ave
        Streets::create(array(
            'name' => 'Galveston Ave',
            'island_id' => 1
        ));

        // Garnet Streets
        Streets::create(array(
            'name' => 'Garnet Streets',
            'island_id' => 1
        ));

        // Grummer Road
        Streets::create(array(
            'name' => 'Grummer Road',
            'island_id' => 1
        ));

        // Hell Gate
        Streets::create(array(
            'name' => 'Hell Gate',
            'island_id' => 1
        ));

        // Hematite Streets
        Streets::create(array(
            'name' => 'Hematite Streets',
            'island_id' => 1
        ));

        // Iron Streets
        Streets::create(array(
            'name' => 'Iron Streets',
            'island_id' => 1
        ));

        // Ivy Drive North
        Streets::create(array(
            'name' => 'Ivy Drive North',
            'island_id' => 1
        ));

        // Ivy Drive South
        Streets::create(array(
            'name' => 'Ivy Drive South',
            'island_id' => 1
        ));

        // Jade Streets
        Streets::create(array(
            'name' => 'Jade Streets',
            'island_id' => 1
        ));

        // Liberty Lane
        Streets::create(array(
            'name' => 'Liberty Lane',
            'island_id' => 1
        ));

        // Lorimar St
        Streets::create(array(
            'name' => 'Lorimar St',
            'island_id' => 1
        ));

        // Luddite Row
        Streets::create(array(
            'name' => 'Luddite Row',
            'island_id' => 1
        ));

        // Kunzite St
        Streets::create(array(
            'name' => 'Kunzite St',
            'island_id' => 1
        ));

        // Manganese St
        Streets::create(array(
            'name' => 'Manganese St',
            'island_id' => 1
        ));

        // Nickel Streets
        Streets::create(array(
            'name' => 'Nickel Streets',
            'island_id' => 1
        ));

        // Obsidian Streets
        Streets::create(array(
            'name' => 'Obsidian Streets',
            'island_id' => 1
        ));

        // President Avenue
        Streets::create(array(
            'name' => 'President Avenue',
            'island_id' => 1
        ));

        // President St
        Streets::create(array(
            'name' => 'President St',
            'island_id' => 1
        ));

        // Privateer Road
        Streets::create(array(
            'name' => 'Privateer Road',
            'island_id' => 1
        ));

        // Pyrite Streets
        Streets::create(array(
            'name' => 'Pyrite Streets',
            'island_id' => 1
        ));

        // Quartz Streets
        Streets::create(array(
            'name' => 'Quartz Streets',
            'island_id' => 1
        ));

        // Ruby St
        Streets::create(array(
            'name' => 'Ruby St',
            'island_id' => 1
        ));

        // Silicon Streets
        Streets::create(array(
            'name' => 'Silicon Streets',
            'island_id' => 1
        ));

        // South Parkway
        Streets::create(array(
            'name' => 'South Parkway',
            'island_id' => 1
        ));

        // Topaz St
        Streets::create(array(
            'name' => 'Topaz St',
            'island_id' => 1
        ));

        // Union Drive East
        Streets::create(array(
            'name' => 'Union Drive East',
            'island_id' => 1
        ));

        // Union Drive West
        Streets::create(array(
            'name' => 'Union Drive West',
            'island_id' => 1
        ));

        // Uranium Streets
        Streets::create(array(
            'name' => 'Uranium Streets',
            'island_id' => 1
        ));

        // Vauxite St
        Streets::create(array(
            'name' => 'Vauxite St',
            'island_id' => 1
        ));

        // Vespucci Circus
        Streets::create(array(
            'name' => 'Vespucci Circus',
            'island_id' => 1
        ));

        // Walnut Way
        Streets::create(array(
            'name' => 'Walnut Way',
            'island_id' => 1
        ));

        // Wardite Streets
        Streets::create(array(
            'name' => 'Wardite Streets',
            'island_id' => 1
        ));

        // West Way
        Streets::create(array(
            'name' => 'West Way',
            'island_id' => 1
        ));

        // Wong Way
        Streets::create(array(
            'name' => 'Wong Way',
            'island_id' => 1
        ));

        // Xenotime St
        Streets::create(array(
            'name' => 'Xenotime St',
            'island_id' => 1
        ));

        // Alcatraz Avenue
        Streets::create(array(
            'name' => 'Alcatraz Avenue',
            'island_id' => 1
        ));

        // Altona Avenue
        Streets::create(array(
            'name' => 'Altona Avenue',
            'island_id' => 1
        ));

        // Applejack Streets
        Streets::create(array(
            'name' => 'Applejack Streets',
            'island_id' => 1
        ));

        // Attica Avenue
        Streets::create(array(
            'name' => 'Attica Avenue',
            'island_id' => 1
        ));

        // Beaumont Ave
        Streets::create(array(
            'name' => 'Beaumont Ave',
            'island_id' => 1
        ));

        // Bronco Streets
        Streets::create(array(
            'name' => 'Bronco Streets',
            'island_id' => 1
        ));

        // Butterfly Streets
        Streets::create(array(
            'name' => 'Butterfly Streets',
            'island_id' => 1
        ));

        // Caterpillar Streets
        Streets::create(array(
            'name' => 'Caterpillar Streets',
            'island_id' => 1
        ));

        // Coxsack Avenue
        Streets::create(array(
            'name' => 'Coxsack Avenue',
            'island_id' => 1
        ));

        // Darkhammer Streets
        Streets::create(array(
            'name' => 'Darkhammer Streets',
            'island_id' => 1
        ));

        // Downrock Loop
        Streets::create(array(
            'name' => 'Downrock Loop',
            'island_id' => 1
        ));

        // Drill Streets
        Streets::create(array(
            'name' => 'Drill Streets',
            'island_id' => 1
        ));

        // Drop Streets
        Streets::create(array(
            'name' => 'Drop Streets',
            'island_id' => 1
        ));

        // Elbow Streets
        Streets::create(array(
            'name' => 'Elbow Streets',
            'island_id' => 1
        ));

        // Flanger Streets
        Streets::create(array(
            'name' => 'Flanger Streets',
            'island_id' => 1
        ));

        // Folsom Way
        Streets::create(array(
            'name' => 'Folsom Way',
            'island_id' => 1
        ));

        // Gainer Streets
        Streets::create(array(
            'name' => 'Gainer Streets',
            'island_id' => 1
        ));

        // Grand Boulevard
        Streets::create(array(
            'name' => 'Grand Boulevard',
            'island_id' => 1
        ));

        // Greene Avenue
        Streets::create(array(
            'name' => 'Greene Avenue',
            'island_id' => 1
        ));

        // Guantanamo Ave
        Streets::create(array(
            'name' => 'Guantanamo Ave',
            'island_id' => 1
        ));

        // Hollowback Streets
        Streets::create(array(
            'name' => 'Hollowback Streets',
            'island_id' => 1
        ));

        // Jackhamer Streets
        Streets::create(array(
            'name' => 'Jackhamer Streets',
            'island_id' => 1
        ));

        // Joliet St
        Streets::create(array(
            'name' => 'Joliet St',
            'island_id' => 1
        ));

        // Leavenworth Avenue
        Streets::create(array(
            'name' => 'Leavenworth Avenue',
            'island_id' => 1
        ));

        // Lompoc Avenue
        Streets::create(array(
            'name' => 'Lompoc Avenue',
            'island_id' => 1
        ));

        // Lotus Streets
        Streets::create(array(
            'name' => 'Lotus Streets',
            'island_id' => 1
        ));

        // Mill Streets
        Streets::create(array(
            'name' => 'Mill Streets',
            'island_id' => 1
        ));

        // Northern Expressway
        Streets::create(array(
            'name' => 'Northern Expressway',
            'island_id' => 1
        ));

        // Planche Avenue
        Streets::create(array(
            'name' => 'Planche Avenue',
            'island_id' => 1
        ));

        // Rocket Streets
        Streets::create(array(
            'name' => 'Rocket Streets',
            'island_id' => 1
        ));

        // Rykers Avenue
        Streets::create(array(
            'name' => 'Rykers Avenue',
            'island_id' => 1
        ));

        // San Quentin Ave
        Streets::create(array(
            'name' => 'San Quentin Ave',
            'island_id' => 1
        ));

        // Sing Sing Avenue
        Streets::create(array(
            'name' => 'Sing Sing Avenue',
            'island_id' => 1
        ));

        // Spin Streets
        Streets::create(array(
            'name' => 'Spin Streets',
            'island_id' => 1
        ));

        // Switch Streets
        Streets::create(array(
            'name' => 'Switch Streets',
            'island_id' => 1
        ));

        // Turtle Streets
        Streets::create(array(
            'name' => 'Turtle Streets',
            'island_id' => 1
        ));

        // Uprock Streets
        Streets::create(array(
            'name' => 'Uprock Streets',
            'island_id' => 1
        ));

        // Valdez Avenue
        Streets::create(array(
            'name' => 'Valdez Avenue',
            'island_id' => 1
        ));

        // Wallkill Avenue
        Streets::create(array(
            'name' => 'Wallkill Avenue',
            'island_id' => 1
        ));

        // Windmill Streets
        Streets::create(array(
            'name' => 'Windmill Streets',
            'island_id' => 1
        ));

        // Worm Streets
        Streets::create(array(
            'name' => 'Worm Streets',
            'island_id' => 1
        ));

        // Algonquin - Dukes Expressway
        Streets::create(array(
            'name' => 'Algonquin - Dukes Expressway',
            'island_id' => 1
        ));

        // Aragon St
        Streets::create(array(
            'name' => 'Aragon St',
            'island_id' => 1
        ));

        // Asparagus Ave
        Streets::create(array(
            'name' => 'Asparagus Ave',
            'island_id' => 1
        ));

        // Bart St
        Streets::create(array(
            'name' => 'Bart St',
            'island_id' => 1
        ));

        // Boone St
        Streets::create(array(
            'name' => 'Boone St',
            'island_id' => 1
        ));

        // Brandon Ave
        Streets::create(array(
            'name' => 'Brandon Ave',
            'island_id' => 1
        ));

        // Broker Bridge
        Streets::create(array(
            'name' => 'Broker Bridge',
            'island_id' => 1
        ));

        // Broker - Dukes Expressway
        Streets::create(array(
            'name' => 'Broker - Dukes Expressway',
            'island_id' => 1
        ));

        // Brunner St
        Streets::create(array(
            'name' => 'Brunner St',
            'island_id' => 1
        ));

        // Bunker Hill Ave
        Streets::create(array(
            'name' => 'Bunker Hill Ave',
            'island_id' => 1
        ));

        // Camden Ave
        Streets::create(array(
            'name' => 'Camden Ave',
            'island_id' => 1
        ));

        // Carrollton St
        Streets::create(array(
            'name' => 'Carrollton St',
            'island_id' => 1
        ));

        // Carson St
        Streets::create(array(
            'name' => 'Carson St',
            'island_id' => 1
        ));

        // Cassidy St
        Streets::create(array(
            'name' => 'Cassidy St',
            'island_id' => 1
        ));

        // Cayuga Ave
        Streets::create(array(
            'name' => 'Cayuga Ave',
            'island_id' => 1
        ));

        // Charleston Ave
        Streets::create(array(
            'name' => 'Charleston Ave',
            'island_id' => 1
        ));

        // Chicory St
        Streets::create(array(
            'name' => 'Chicory St',
            'island_id' => 1
        ));

        // Chive St
        Streets::create(array(
            'name' => 'Chive St',
            'island_id' => 1
        ));

        // Cisco St
        Streets::create(array(
            'name' => 'Cisco St',
            'island_id' => 1
        ));

        // Claiborne St
        Streets::create(array(
            'name' => 'Claiborne St',
            'island_id' => 1
        ));

        // Cleves Ave
        Streets::create(array(
            'name' => 'Cleves Ave',
            'island_id' => 1
        ));

        // Cody St
        Streets::create(array(
            'name' => 'Cody St',
            'island_id' => 1
        ));

        // Concord Ave
        Streets::create(array(
            'name' => 'Concord Ave',
            'island_id' => 1
        ));

        // Conoy Ave
        Streets::create(array(
            'name' => 'Conoy Ave',
            'island_id' => 1
        ));

        // Creek St
        Streets::create(array(
            'name' => 'Creek St',
            'island_id' => 1
        ));

        // Crockett Ave
        Streets::create(array(
            'name' => 'Crockett Ave',
            'island_id' => 1
        ));

        // Deadwood St
        Streets::create(array(
            'name' => 'Deadwood St',
            'island_id' => 1
        ));

        // Delaware Ave
        Streets::create(array(
            'name' => 'Delaware Ave',
            'island_id' => 1
        ));

        // Dillon St
        Streets::create(array(
            'name' => 'Dillon St',
            'island_id' => 1
        ));

        // Dukes Drive
        Streets::create(array(
            'name' => 'Dukes Drive',
            'island_id' => 1
        ));

        // Dukes Boulevard
        Streets::create(array(
            'name' => 'Dukes Boulevard',
            'island_id' => 1
        ));

        // Earp St
        Streets::create(array(
            'name' => 'Earp St',
            'island_id' => 1
        ));

        // Ellery St
        Streets::create(array(
            'name' => 'Ellery St',
            'island_id' => 1
        ));

        // Erie Ave
        Streets::create(array(
            'name' => 'Erie Ave',
            'island_id' => 1
        ));

        // Franklin St
        Streets::create(array(
            'name' => 'Franklin St',
            'island_id' => 1
        ));

        // Freetown Ave
        Streets::create(array(
            'name' => 'Freetown Ave',
            'island_id' => 1
        ));

        // Garrett St
        Streets::create(array(
            'name' => 'Garrett St',
            'island_id' => 1
        ));

        // Gibson St
        Streets::create(array(
            'name' => 'Gibson St',
            'island_id' => 1
        ));

        // Hancock St
        Streets::create(array(
            'name' => 'Hancock St',
            'island_id' => 1
        ));

        // Hardin St
        Streets::create(array(
            'name' => 'Hardin St',
            'island_id' => 1
        ));

        // Harrison St
        Streets::create(array(
            'name' => 'Harrison St',
            'island_id' => 1
        ));

        // Hewes St
        Streets::create(array(
            'name' => 'Hewes St',
            'island_id' => 1
        ));

        // Hickock St
        Streets::create(array(
            'name' => 'Hickock St',
            'island_id' => 1
        ));

        // Hooper St
        Streets::create(array(
            'name' => 'Hooper St',
            'island_id' => 1
        ));

        // Howard St
        Streets::create(array(
            'name' => 'Howard St',
            'island_id' => 1
        ));

        // Huntington St
        Streets::create(array(
            'name' => 'Huntington St',
            'island_id' => 1
        ));

        // Inchon Ave
        Streets::create(array(
            'name' => 'Inchon Ave',
            'island_id' => 1
        ));

        // Iroquois Ave
        Streets::create(array(
            'name' => 'Iroquois Ave',
            'island_id' => 1
        ));

        // James St
        Streets::create(array(
            'name' => 'James St',
            'island_id' => 1
        ));

        // Ketchum St
        Streets::create(array(
            'name' => 'Ketchum St',
            'island_id' => 1
        ));

        // Kid St
        Streets::create(array(
            'name' => 'Kid St',
            'island_id' => 1
        ));

        // Livingston St
        Streets::create(array(
            'name' => 'Livingston St',
            'island_id' => 1
        ));

        // Lynch St
        Streets::create(array(
            'name' => 'Lynch St',
            'island_id' => 1
        ));

        // Masterson St
        Streets::create(array(
            'name' => 'Masterson St',
            'island_id' => 1
        ));

        // Middleton Lane
        Streets::create(array(
            'name' => 'Middleton Lane',
            'island_id' => 1
        ));

        // Mohanet Ave
        Streets::create(array(
            'name' => 'Mohanet Ave',
            'island_id' => 1
        ));

        // Mohawk Ave
        Streets::create(array(
            'name' => 'Mohawk Ave',
            'island_id' => 1
        ));

        // Mohegan Ave
        Streets::create(array(
            'name' => 'Mohegan Ave',
            'island_id' => 1
        ));

        // Montauk Ave
        Streets::create(array(
            'name' => 'Montauk Ave',
            'island_id' => 1
        ));

        // Morris St
        Streets::create(array(
            'name' => 'Morris St',
            'island_id' => 1
        ));

        // Munsee Ave
        Streets::create(array(
            'name' => 'Munsee Ave',
            'island_id' => 1
        ));

        // Oakley St
        Streets::create(array(
            'name' => 'Oakley St',
            'island_id' => 1
        ));

        // Oneida Ave
        Streets::create(array(
            'name' => 'Oneida Ave',
            'island_id' => 1
        ));

        // Onion St
        Streets::create(array(
            'name' => 'Onion St',
            'island_id' => 1
        ));

        // Onondaga Ave
        Streets::create(array(
            'name' => 'Onondaga Ave',
            'island_id' => 1
        ));

        // Pancho St
        Streets::create(array(
            'name' => 'Pancho St',
            'island_id' => 1
        ));

        // Parr St
        Streets::create(array(
            'name' => 'Parr St',
            'island_id' => 1
        ));

        // Ringo St
        Streets::create(array(
            'name' => 'Ringo St',
            'island_id' => 1
        ));

        // San Jacinto Ave
        Streets::create(array(
            'name' => 'San Jacinto Ave',
            'island_id' => 1
        ));

        // Saponi Ave
        Streets::create(array(
            'name' => 'Saponi Ave',
            'island_id' => 1
        ));

        // Saratoga Avenue
        Streets::create(array(
            'name' => 'Saratoga Avenue',
            'island_id' => 1
        ));

        // Savannah Ave
        Streets::create(array(
            'name' => 'Savannah Ave',
            'island_id' => 1
        ));

        // Seneca Ave
        Streets::create(array(
            'name' => 'Seneca Ave',
            'island_id' => 1
        ));

        // Seymour Ave
        Streets::create(array(
            'name' => 'Seymour Ave',
            'island_id' => 1
        ));

        // Shinnecock Ave
        Streets::create(array(
            'name' => 'Shinnecock Ave',
            'island_id' => 1
        ));

        // Starr St
        Streets::create(array(
            'name' => 'Starr St',
            'island_id' => 1
        ));

        // Stillwater Ave
        Streets::create(array(
            'name' => 'Stillwater Ave',
            'island_id' => 1
        ));

        // Stone St
        Streets::create(array(
            'name' => 'Stone St',
            'island_id' => 1
        ));

        // Sundance St
        Streets::create(array(
            'name' => 'Sundance St',
            'island_id' => 1
        ));

        // Thornton Streets
        Streets::create(array(
            'name' => 'Thornton Streets',
            'island_id' => 1
        ));

        // Tinconderoga Ave
        Streets::create(array(
            'name' => 'Tinconderoga Ave',
            'island_id' => 1
        ));

        // Trenton Ave
        Streets::create(array(
            'name' => 'Trenton Ave',
            'island_id' => 1
        ));

        // Tulsa St
        Streets::create(array(
            'name' => 'Tulsa St',
            'island_id' => 1
        ));

        // Tudor St
        Streets::create(array(
            'name' => 'Tudor St',
            'island_id' => 1
        ));

        // Tuscarora Ave
        Streets::create(array(
            'name' => 'Tuscarora Ave',
            'island_id' => 1
        ));

        // Tutelo Ave
        Streets::create(array(
            'name' => 'Tutelo Ave',
            'island_id' => 1
        ));

        // Valley Forge Ave
        Streets::create(array(
            'name' => 'Valley Forge Ave',
            'island_id' => 1
        ));

        // Walton Lane
        Streets::create(array(
            'name' => 'Walton Lane',
            'island_id' => 1
        ));

        // Wappinger Ave
        Streets::create(array(
            'name' => 'Wappinger Ave',
            'island_id' => 1
        ));

        // Wenrohronon Ave
        Streets::create(array(
            'name' => 'Wenrohronon Ave',
            'island_id' => 1
        ));

        // Yorktown Ave
        Streets::create(array(
            'name' => 'Yorktown Ave',
            'island_id' => 1
        ));
    }
}