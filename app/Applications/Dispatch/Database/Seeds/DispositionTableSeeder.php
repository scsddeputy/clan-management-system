<?php
/**
 * Copyright StreamersIO LLC.
 * User: Josh
 * Date: 12/12/2015
 * Time: 11:59 AM
 */

namespace App\Applications\Dispatch\Database\Seeds;


use App\Applications\Dispatch\Models\Dispositions;
use Illuminate\Database\Seeder;

class DispositionTableSeeder extends Seeder
{
    public function run()
    {
        Dispositions::create([
            'name' => 'Pending',
            'user_id' => 1
        ]);

        Dispositions::create([
            'name' => 'Cleared',
            'user_id' => 1
        ]);
    }
}