<?php
/**
 * Copyright StreamersIO LLC.
 * User: Josh
 * Date: 12/12/2015
 * Time: 12:04 PM
 */

namespace App\Applications\Dispatch\Database\Seeds;


use App\Applications\Dispatch\Models\IncidentTypes;
use Illuminate\Database\Seeder;

class IncidentTypeTableSeeder extends Seeder
{
    public function run()
    {
        IncidentTypes::create([
            'name' => 'Backup Request',
            'user_id' => 1
        ]);

        IncidentTypes::create([
            'name' => 'Shots Fired',
            'user_id' => 1
        ]);
    }
}