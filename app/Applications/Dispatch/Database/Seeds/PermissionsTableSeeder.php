<?php
/**
 * Copyright StreamersIO LLC.
 * User: Josh
 * Date: 12/12/2015
 * Time: 12:06 PM
 */

namespace App\Applications\Dispatch\Database\Seeds;


use App\Models\Access\Permission\Permission;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $group_model        = config('access.group');
        $groupDispatch             = new $group_model;
        $groupDispatch->app        = 'dispatch';
        $groupDispatch->name       = 'Dispatch';
        $groupDispatch->sort       = 1;
        $groupDispatch->created_at = Carbon::now();
        $groupDispatch->updated_at = Carbon::now();
        $groupDispatch->save();

        $groupIncident = new $group_model;
        $groupIncident->name = 'Incidents';
        $groupIncident->sort = 2;
        $groupIncident->parent_id = $groupDispatch->id;
        $groupIncident->app        = 'dispatch';
        $groupIncident->created_at = Carbon::now();
        $groupIncident->updated_at = Carbon::now();
        $groupIncident->save();

        $groupUnits = new $group_model;
        $groupUnits->name = 'Units';
        $groupUnits->sort = 3;
        $groupUnits->parent_id = $groupDispatch->id;
        $groupUnits->app        = 'dispatch';
        $groupUnits->created_at = Carbon::now();
        $groupUnits->updated_at = Carbon::now();
        $groupUnits->save();

        $groupNcic = new $group_model;
        $groupNcic->name = 'NCIC';
        $groupNcic->sort = 4;
        $groupNcic->parent_id = $groupDispatch->id;
        $groupNcic->app        = 'dispatch';
        $groupNcic->created_at = Carbon::now();
        $groupNcic->updated_at = Carbon::now();
        $groupNcic->save();

        $groupRadio = new $group_model;
        $groupRadio->name = 'Radio';
        $groupRadio->sort = 5;
        $groupRadio->parent_id = $groupDispatch->id;
        $groupRadio->app        = 'dispatch';
        $groupRadio->created_at = Carbon::now();
        $groupRadio->updated_at = Carbon::now();
        $groupRadio->save();

        $permission_model          = config('access.permission');
        $accessDispatch               = new $permission_model;
        $accessDispatch->name         = 'access-dispatch';
        $accessDispatch->display_name = 'Access Dispatch';
        $accessDispatch->system       = true;
        $accessDispatch->group_id     = $groupIncident->id;
        $accessDispatch->app        = 'dispatch';
        $accessDispatch->sort         = 1;
        $accessDispatch->created_at   = Carbon::now();
        $accessDispatch->updated_at   = Carbon::now();
        $accessDispatch->save();

        $permission_model          = config('access.permission');
        $viewIncident               = new $permission_model;
        $viewIncident->name         = 'view-incident';
        $viewIncident->display_name = 'View Incident';
        $viewIncident->system       = true;
        $viewIncident->group_id     = $groupIncident->id;
        $viewIncident->sort         = 1;
        $viewIncident->app        = 'dispatch';
        $viewIncident->created_at   = Carbon::now();
        $viewIncident->updated_at   = Carbon::now();
        $viewIncident->save();

        $permission_model          = config('access.permission');
        $createIncident               = new $permission_model;
        $createIncident->name         = 'create-incident';
        $createIncident->display_name = 'Create Incident';
        $createIncident->system       = true;
        $createIncident->group_id     = $groupIncident->id;
        $createIncident->sort         = 2;
        $createIncident->app        = 'dispatch';
        $createIncident->created_at   = Carbon::now();
        $createIncident->updated_at   = Carbon::now();
        $createIncident->save();


        $permission_model          = config('access.permission');
        $editIncident               = new $permission_model;
        $editIncident->name         = 'edit-incident';
        $editIncident->display_name = 'Edit Incident';
        $editIncident->system       = true;
        $editIncident->group_id     = $groupIncident->id;
        $editIncident->sort         = 3;
        $editIncident->app        = 'dispatch';
        $editIncident->created_at   = Carbon::now();
        $editIncident->updated_at   = Carbon::now();
        $editIncident->save();


        $permission_model          = config('access.permission');
        $editIncident               = new $permission_model;
        $editIncident->name         = 'view-incident-history';
        $editIncident->display_name = 'View Incident History';
        $editIncident->system       = true;
        $editIncident->group_id     = $groupIncident->id;
        $editIncident->sort         = 4;
        $editIncident->app        = 'dispatch';
        $editIncident->created_at   = Carbon::now();
        $editIncident->updated_at   = Carbon::now();
        $editIncident->save();


        $permission_model          = config('access.permission');
        $manageDuty               = new $permission_model;
        $manageDuty->name         = 'manage-duty-status';
        $manageDuty->display_name = 'Manage Unit Duty Status';
        $manageDuty->system       = true;
        $manageDuty->group_id     = $groupUnits->id;
        $manageDuty->sort         = 1;
        $manageDuty->app        = 'dispatch';
        $manageDuty->created_at   = Carbon::now();
        $manageDuty->updated_at   = Carbon::now();
        $manageDuty->save();


        $permission_model          = config('access.permission');
        $manageAvailable               = new $permission_model;
        $manageAvailable->name         = 'manage-available-status';
        $manageAvailable->display_name = 'Manage Unit Availability';
        $manageAvailable->system       = true;
        $manageAvailable->group_id     = $groupUnits->id;
        $manageAvailable->sort         = 2;
        $manageAvailable->app        = 'dispatch';
        $manageAvailable->created_at   = Carbon::now();
        $manageAvailable->updated_at   = Carbon::now();
        $manageAvailable->save();


        $permission_model          = config('access.permission');
        $radioView               = new $permission_model;
        $radioView->name         = 'view-radio-log';
        $radioView->display_name = 'View Radio Log';
        $radioView->system       = true;
        $radioView->group_id     = $groupRadio->id;
        $radioView->sort         = 1;
        $radioView->app        = 'dispatch';
        $radioView->created_at   = Carbon::now();
        $radioView->updated_at   = Carbon::now();
        $radioView->save();

        $permission_model          = config('access.permission');
        $radioSend               = new $permission_model;
        $radioSend->name         = 'send-radio-message';
        $radioSend->display_name = 'Send Radio Message';
        $radioSend->system       = true;
        $radioSend->group_id     = $groupRadio->id;
        $radioSend->sort         = 2;
        $radioSend->app        = 'dispatch';
        $radioSend->created_at   = Carbon::now();
        $radioSend->updated_at   = Carbon::now();
        $radioSend->save();



    }
}