<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDispatchIncidentsTable extends Migration {

	public function up()
	{
		Schema::create('dispatch_incidents', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('incident_type_id')->unsigned()->nullable();
			$table->integer('street_id')->unsigned()->nullable();
			$table->integer('cross_id')->unsigned()->nullable();
			$table->time('pd_dispatched')->nullable();
			$table->time('pd_arrived')->nullable();
			$table->time('pd_clear')->nullable();
			$table->time('fd_dispatched')->nullable();
			$table->time('fd_arrived')->nullable();
			$table->time('fd_clear')->nullable();
			$table->integer('disposition')->unsigned();
			$table->boolean('incident_cleared')->default(0);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('dispatch_incidents');
	}
}