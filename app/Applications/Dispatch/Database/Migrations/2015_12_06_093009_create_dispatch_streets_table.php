<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDispatchStreetsTable extends Migration {

	public function up()
	{
		Schema::create('dispatch_streets', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('island_id')->unsigned()->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('dispatch_streets');
	}
}