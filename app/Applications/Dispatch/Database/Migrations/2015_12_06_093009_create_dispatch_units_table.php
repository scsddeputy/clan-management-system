<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDispatchUnitsTable extends Migration {

	public function up()
	{
		Schema::create('dispatch_units', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('unit_number');
			$table->integer('division_id')->unsigned()->nullable();
			$table->boolean('call_status')->default(0);
			$table->boolean('duty_status')->default(0);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('dispatch_units');
	}
}