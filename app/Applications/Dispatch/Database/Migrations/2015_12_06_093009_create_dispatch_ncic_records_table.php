<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDispatchNcicRecordsTable extends Migration {

	public function up()
	{
		Schema::create('dispatch_ncic_records', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('person_id')->unsigned();
			$table->integer('user_id')->unsigned()->nullable();
			$table->enum('record_type', array('tracker', 'ticket', 'warrant', 'arrest'));
			$table->text('record_info')->nullable();
			$table->boolean('record_closed')->default(0);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('dispatch_ncic_records');
	}
}