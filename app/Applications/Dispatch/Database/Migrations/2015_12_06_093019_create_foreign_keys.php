<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('dispatch_incidents', function(Blueprint $table) {
			$table->foreign('incident_type_id')->references('id')->on('dispatch_incident_types')
						->onDelete('set null')
						->onUpdate('no action');
		});
		Schema::table('dispatch_incidents', function(Blueprint $table) {
			$table->foreign('street_id')->references('id')->on('dispatch_streets')
						->onDelete('set null')
						->onUpdate('no action');
		});
		Schema::table('dispatch_incidents', function(Blueprint $table) {
			$table->foreign('cross_id')->references('id')->on('dispatch_streets')
						->onDelete('set null')
						->onUpdate('no action');
		});
		Schema::table('dispatch_incidents', function(Blueprint $table) {
			$table->foreign('disposition')->references('id')->on('dispatch_dispositions')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('dispatch_incident_notes', function(Blueprint $table) {
			$table->foreign('incident_id')->references('id')->on('dispatch_incidents')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('dispatch_incident_notes', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('dispatch_dispositions', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('set null')
						->onUpdate('no action');
		});
		Schema::table('dispatch_incident_types', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('set null')
						->onUpdate('no action');
		});
		Schema::table('dispatch_streets', function(Blueprint $table) {
			$table->foreign('island_id')->references('id')->on('dispatch_islands')
						->onDelete('set null')
						->onUpdate('no action');
		});
		Schema::table('dispatch_ncic_records', function(Blueprint $table) {
			$table->foreign('person_id')->references('id')->on('dispatch_persons')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('dispatch_ncic_records', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('set null')
						->onUpdate('no action');
		});
		Schema::table('dispatch_radio_log', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('dispatch_units', function(Blueprint $table) {
			$table->foreign('division_id')->references('id')->on('dispatch_divisions')
						->onDelete('set null')
						->onUpdate('no action');
		});
		Schema::table('dispatch_incident_units', function(Blueprint $table) {
			$table->foreign('incident_id')->references('id')->on('dispatch_incidents')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('dispatch_incident_units', function(Blueprint $table) {
			$table->foreign('unit_id')->references('id')->on('dispatch_units')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('dispatch_incidents', function(Blueprint $table) {
			$table->dropForeign('dispatch_incidents_incident_type_id_foreign');
		});
		Schema::table('dispatch_incidents', function(Blueprint $table) {
			$table->dropForeign('dispatch_incidents_street_id_foreign');
		});
		Schema::table('dispatch_incidents', function(Blueprint $table) {
			$table->dropForeign('dispatch_incidents_cross_id_foreign');
		});
		Schema::table('dispatch_incidents', function(Blueprint $table) {
			$table->dropForeign('dispatch_incidents_disposition_foreign');
		});
		Schema::table('dispatch_incident_notes', function(Blueprint $table) {
			$table->dropForeign('dispatch_incident_notes_incident_id_foreign');
		});
		Schema::table('dispatch_incident_notes', function(Blueprint $table) {
			$table->dropForeign('dispatch_incident_notes_user_id_foreign');
		});
		Schema::table('dispatch_dispositions', function(Blueprint $table) {
			$table->dropForeign('dispatch_dispositions_user_id_foreign');
		});
		Schema::table('dispatch_incident_types', function(Blueprint $table) {
			$table->dropForeign('dispatch_incident_types_user_id_foreign');
		});
		Schema::table('dispatch_streets', function(Blueprint $table) {
			$table->dropForeign('dispatch_streets_island_id_foreign');
		});
		Schema::table('dispatch_ncic_records', function(Blueprint $table) {
			$table->dropForeign('dispatch_ncic_records_person_id_foreign');
		});
		Schema::table('dispatch_ncic_records', function(Blueprint $table) {
			$table->dropForeign('dispatch_ncic_records_user_id_foreign');
		});
		Schema::table('dispatch_radio_log', function(Blueprint $table) {
			$table->dropForeign('dispatch_radio_log_user_id_foreign');
		});
		Schema::table('dispatch_units', function(Blueprint $table) {
			$table->dropForeign('dispatch_units_division_id_foreign');
		});
		Schema::table('dispatch_incident_units', function(Blueprint $table) {
			$table->dropForeign('dispatch_incident_units_incident_id_foreign');
		});
		Schema::table('dispatch_incident_units', function(Blueprint $table) {
			$table->dropForeign('dispatch_incident_units_unit_id_foreign');
		});
	}
}