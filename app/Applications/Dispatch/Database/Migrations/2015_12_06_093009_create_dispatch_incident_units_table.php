<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDispatchIncidentUnitsTable extends Migration {

	public function up()
	{
		Schema::create('dispatch_incident_units', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('incident_id')->unsigned();
			$table->integer('unit_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('dispatch_incident_units');
	}
}