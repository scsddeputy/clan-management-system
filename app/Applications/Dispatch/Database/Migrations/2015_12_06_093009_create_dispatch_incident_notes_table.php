<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDispatchIncidentNotesTable extends Migration {

	public function up()
	{
		Schema::create('dispatch_incident_notes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('incident_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->text('note');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('dispatch_incident_notes');
	}
}