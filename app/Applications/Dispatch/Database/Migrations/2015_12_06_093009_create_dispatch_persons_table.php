<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDispatchPersonsTable extends Migration {

	public function up()
	{
		Schema::create('dispatch_persons', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('dispatch_persons');
	}
}