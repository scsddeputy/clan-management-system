<?php namespace App\Http\Composers;

use Caffeinated\Modules\Modules;
use Illuminate\Contracts\View\View;


class FrontendComposer
{
    protected $modules;

    /**
     * FrontendComposer constructor.
     * @param $modules
     */
    public function __construct(Modules $modules)
    {
        $this->modules = $modules;
    }


    public function compose(View $view)
    {
        $view->with('modules', $this->modules->all());
    }

}