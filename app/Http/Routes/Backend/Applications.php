<?php

Route::group(['prefix' => 'applications'], function ()
{
    get('/', 'DashboardController@apps');
});